"use strict";

function range(start, end){
  if (end < start) {
    return [];
  } else if (start === end) {
    return [start];
  }

  var prevRange = range(start, end - 1);
  prevRange.push(end);
  return prevRange;
};

function recSum(array) {
  if (array.length === 0) {
    return 0;
  } else if (array.length === 1) {
    return array[0];
  }

  var prevSum = recSum(array.slice(1));
  return (prevSum + array[0]);
};

function itSum(array) {
  var sum = 0;
  for (var i = 0; i < array.length; i++) {
    sum += array[i];
  }
  return sum;
};

function exponentA(base, power){
  if (power === 0){
    return 1;
  }else if (power === 1){
    return base;
  }
  var prevExp = exponentA(base, power-1);
  return (prevExp * base);
};

function exponentB(base, power){

  if (power === 0){
    return 1;
  }else if (power === 1){
    return base;
  }
  if (power % 2 === 0) {
    prevExp = exponentB(base, power / 2);
    return (prevExp * prevExp);
  }else{
    prevExp = exponentB(base, power-1);
    return (prevExp * base);
  }
};

function recFib(n) {
  if (n === 0) {
    return [];
  } else if (n === 1) {
    return [0];
  } else if (n === 2) {
    return [0, 1];
  }

  var prevFib = recFib(n - 1);
  prevFib.push(prevFib[prevFib.length - 1] + prevFib[prevFib.length - 2]);

  return prevFib;
};

function itFib(n) {
  if (n === 0) {
    return [];
  } else if (n === 1) {
    return [0];
  }

  var fibArray = [0, 1];

  var count = 2;
  while (count < n) {
    fibArray.push(fibArray[fibArray.length - 1] + fibArray[fibArray.length - 2]);
    count++;
  }

  return fibArray;
};

function bsearch(array, target) {
  var middle = Math.floor(array.length / 2);
  if (array.length === 0) {
    return NaN;
  }
  if (array[middle] === target) {
    return middle;
  }else if(array[middle] > target){
    var leftSearch = bsearch(array.slice(0, middle), target);
    return leftSearch;
  }else if (array[middle] < target) {
    var rightSearch = bsearch(array.slice(middle+1), target);
    return (middle + 1 + rightSearch);
  }
};

function makeChange(amt, coins) {
  for (var i = 0; i < coins.length; i++) {
    if (amt === coins[i]) {
      return [coins[i]];
    }
  }
  var bestChange = null;
  for (var i = 0; i < coins.length; i++) {
    if(coins[i] > amt) {
      continue;
    }
    possibleChange = makeChange(amt - coins[i], coins.slice(i));
    possibleChange.push(coins[i]);

    if ((bestChange === null) || (bestChange.length > possibleChange.length)) {
      bestChange = possibleChange;
    }
  }

  return bestChange;
};

function mergeSort(array){
  if (array.length === 0 || array.length === 1){
    return array;
  }
  var middle = Math.floor(array.length / 2);
  var left = array.slice(0, middle);
  var right = array.slice(middle);
  return (merge(mergeSort(left), mergeSort(right)));
};

function merge(left, right){
  var mergedArray = [];
  while (left.length > 0 || right.length > 0) {
    if (left.length === 0){
      mergedArray.push(right.shift());
    }else if (right.length === 0) {
      mergedArray.push(left.shift());
    }else if (left[0] < right[0]) {
      mergedArray.push(left.shift());
    }else {
      mergedArray.push(right.shift());
    }
  }
  return mergedArray;
};

function subsets(array) {
  if (array.length === 0) {
    return [[]];
  }

  var prevSubsets = subsets(array.slice(0, array.length - 1));
  var length = prevSubsets.length
  for (var i = 0; i < length; i++) {
    var last = array[array.length - 1];
    var newSubset = prevSubsets[i].concat(last);

    prevSubsets[prevSubsets.length] = newSubset;
  }

  return prevSubsets;
};

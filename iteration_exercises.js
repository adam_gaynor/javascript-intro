"use strict";
function bubbleSort(array){
  var sorted = false;
  while (!sorted){
    sorted = true;
    for(var i = 0; i < array.length-1; i++){
      var temp;
      if (array[i] > array[i+1]){
        temp = array[i];
        array[i] = array[i+1];
        array[i+1] = temp;
        sorted = false;
      }
    }
  }

  return array;
};

function substrings(string){
  substringsArray = [];
  for (var first = 0; first < string.length; first++) {
    for (var last = first+1; last <= string.length; last++) {
      substringsArray.push(string.slice(first, last));
    }
  }
  return myUniq(substringsArray);
};

function myUniq(array) {
  var uniqArray = [];
  for (var i = 0; i < array.length; i++) {
    if (uniqArray.indexOf(array[i]) === -1) {
      uniqArray[uniqArray.length] = array[i];
    }
  }

  return uniqArray;
};

"use strict";

Array.prototype.myEach = function(func) {
  for (var i = 0; i < this.length; i++) {
    func(this[i]);
  };

  return this;
};

Array.prototype.myMap = function(func) {
  var newArray = [];
  this.myEach(function(num){
    newArray[newArray.length] = func(num);
  });

  return newArray;
};

Array.prototype.myInject = function(func) {
  var accumulator = this[0];
  var arraySlice = this.slice(1);

  arraySlice.myEach(function(num) {
    accumulator = func(accumulator, num);
  });

  return accumulator;
};

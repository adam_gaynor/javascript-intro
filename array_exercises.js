"use strict";

var testArray1 = [-1, 0, 2, -2, 1, 2];
var rows = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8]
  ];

function myUniq(array) {
  var uniqArray = [];
  for (var i = 0; i < array.length; i++) {
    if (uniqArray.indexOf(array[i]) === -1) {
      uniqArray[uniqArray.length] = array[i];
    }
  }

  return uniqArray;
};

function twoSum(array) {
  var twoSums = [];

  for (var i = 0; i < array.length - 1; i ++) {
    for (var j = i + 1; j < array.length; j++) {
      if (array[i] + array[j] === 0) {
        twoSums[twoSums.length] = [i, j];
      }
    }
  }

  return twoSums;
};

function myTranspose(array) {
  var transposedArray = [];

  for(var i = 0; i < array[0].length; i++){
    transposedArray[transposedArray.length] = [];
  }

  for(var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      transposedArray[j][i] = array[i][j];
    }
  }

  return transposedArray;
};
